import random

import pygame as pg

pg.init()

WIDTH = 800
HEIGHT = 600
SIZE = (WIDTH, HEIGHT)
screen = pg.display.set_mode(SIZE)
clock = pg.time.Clock()

# colors
GREEN_COLOR = (0, 240, 0)
GRAY_COLOR = (30, 30, 30)
RED_COLOR = (240, 0, 0)
# bg
bg = pg.image.load("assets/Background/parallax-space-backgound.jpg").convert()
bg = pg.transform.scale(bg, SIZE)

# surface
surface_green = pg.Surface((30, 40))
surface_green.fill(GREEN_COLOR)

# rect
rect_player = pg.Rect(400, 300, 30, 45)
brick_list = []
for i in range(15):
    x = random.randrange(30, WIDTH - 30)
    y = random.randrange(45, HEIGHT - 45)
    brick = pg.Rect(x, y, 30, 45)
    brick_list.append(brick)

# main loop

running = True
while running:
    # do something

    # events
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False

    # update
    old_player_pos = rect_player.center
    rect_player.center = pg.mouse.get_pos()
    # check collision
    for brick in brick_list:
        if brick.colliderect(rect_player):
            rect_player.center = old_player_pos

    # Draw
    screen.fill(GRAY_COLOR)
    # pg.draw.rect(screen, GREEN_COLOR, rect_player)

    for brick in brick_list:
        pg.draw.rect(screen, GREEN_COLOR, brick)

    pg.draw.rect(screen, RED_COLOR, rect_player)
    pg.display.flip()
    clock.tick(30)

pg.quit()
