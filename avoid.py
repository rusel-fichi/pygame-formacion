import random

import pygame as pg

from sprites import Player

pg.mixer.pre_init(44100, -16, 1, 512)
pg.init()

WIDTH = 800
HEIGHT = 600
SIZE = (WIDTH, HEIGHT)
screen = pg.display.set_mode(SIZE)
clock = pg.time.Clock()

# colors
GREEN_COLOR = (0, 240, 0)
GRAY_COLOR = (30, 30, 30)
RED_COLOR = (240, 0, 0)

# constants

# bg
bg = pg.image.load("assets/Background/parallax-space-backgound.jpg").convert()
bg = pg.transform.scale(bg, SIZE)

# img
pointer_img = pg.image.load("assets/crosshairs/cross/cross-01.png").convert_alpha()
pointer_img = pg.transform.scale(pointer_img, (45, 45))

asteroid_img = pg.image.load("assets/Asteroids/asteroid-brown.png").convert_alpha()
asteroid_img = pg.transform.scale(asteroid_img, (45, 45))

player_img = pg.image.load("assets/Spaceships/ship.png").convert_alpha()
player_img = pg.transform.scale(player_img, (45, 45))

# sounds
explosion_sound = pg.mixer.Sound("assets/Sounds/explosionCrunch_001.ogg")

# font
font = pg.font.Font(None, 20)

# Player
player = Player(200, 300, player_img)
player_position_x = 0
player_position_y = 0
player_speed = 10

# group
all_sprites = pg.sprite.Group()

# add sprite to group
all_sprites.add(player)


asteroid_list = []
for i in range(15):
    x = random.randrange(30, WIDTH - 30)
    y = random.randrange(45, HEIGHT - 45)
    brick = pg.Rect(x, y, 30, 45)
    asteroid_list.append(brick)


# main loop

running = True
while running:

    # events
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_LEFT:
                player_position_x -= player_speed
            if event.key == pg.K_RIGHT:
                player_position_x += player_speed
            if event.key == pg.K_UP:
                player_position_y -= player_speed
            if event.key == pg.K_DOWN:
                player_position_y += player_speed


        # if event.type == pg.KEYUP:

    # update
    # player.rect.move_ip(player_position_x, player_position_y)

    all_sprites.update(player_position_x, player_position_y)
    # check collision




    # Draw
    screen.blit(bg, (0, 0))

    all_sprites.draw(screen)

    pg.display.flip()
    clock.tick(30)

pg.quit()