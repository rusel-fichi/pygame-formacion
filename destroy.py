import random

import pygame as pg


pg.mixer.pre_init(44100, -16, 1, 512)
pg.init()

WIDTH = 800
HEIGHT = 600
SIZE = (WIDTH, HEIGHT)
screen = pg.display.set_mode(SIZE)
clock = pg.time.Clock()

# colors
GREEN_COLOR = (0, 240, 0)
GRAY_COLOR = (30, 30, 30)
RED_COLOR = (240, 0, 0)

# constants
kill_count = 0

# bg
bg = pg.image.load("assets/Background/parallax-space-backgound.jpg").convert()
bg = pg.transform.scale(bg, SIZE)

# img
pointer_img = pg.image.load("assets/crosshairs/cross/cross-01.png").convert_alpha()
pointer_img = pg.transform.scale(pointer_img, (45, 45))

asteroid_img = pg.image.load("assets/Asteroids/asteroid-brown.png").convert_alpha()
asteroid_img = pg.transform.scale(asteroid_img, (45, 45))

# sounds
explosion_sound = pg.mixer.Sound("assets/Sounds/explosionCrunch_001.ogg")

# font
font = pg.font.Font(None, 20)

# rect
pointer = pg.Rect(400, 300, 30, 45)
asteroid_list = []
for i in range(15):
    x = random.randrange(30, WIDTH - 30)
    y = random.randrange(45, HEIGHT - 45)
    brick = pg.Rect(x, y, 30, 45)
    asteroid_list.append(brick)

# main loop

running = True
while running:

    # events
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        if event.type == pg.MOUSEBUTTONDOWN:
            for asteroid in asteroid_list[:]:
                if asteroid.colliderect(pointer):
                    kill_count += 1
                    explosion_sound.play()
                    asteroid_list.remove(asteroid)

    # update
    pointer.center = pg.mouse.get_pos()
    pg.mouse.set_visible(False)

    # check collision




    # Draw
    screen.blit(bg, (0, 0))

    for brick in asteroid_list:
        screen.blit(asteroid_img, brick)

    screen.blit(pointer_img, pointer)
    kill_count_text = font.render(f"Kill count: {kill_count}", 0, (255, 255, 255))
    screen.blit(kill_count_text, (350, 10))
    time_count = int(pg.time.get_ticks() / 1000)
    count_time_text = font.render(f"Tiempo: {time_count}", 0, (255, 255, 255))
    screen.blit(count_time_text, (350, 20))

    pg.display.flip()
    clock.tick(30)

pg.quit()