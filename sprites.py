import pygame as pg


class Player(pg.sprite.Sprite):
    def __init__(self, x, y, image):
        super().__init__()
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self, x, y):
        self.rect.move_ip(x, y)
        if self.rect.x > 800:
            self.rect.x = 0
        if self.rect.x < 0:
            self.rect.x = 800


class Asteroid(pg.sprite.Sprite):
    def __init__(self, image):
        super().__init__()
        self.image = image
        self.rect = self.image.get_rect()
